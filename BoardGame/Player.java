import java.util.Scanner;
//----------------------------------------------> player class not in use anymore. i did not realize that there was a Play Class for user inputs 
public class Player {
	/** 
	    * This is the main method which makes use of Player Class - this is temporary it will be removed later
	    * @param args Unused. 
	    * @return Nothing. 
	    */
	public static void main(String args[]) {
		Player player = new Player()	;
			player.setKey();
			player.setPath();
			player.setOrder();
			
			System.out.println("\n");
			System.out.println("Key Entered: "+player.getKey());
			System.out.println("Path Entered: "+player.getPath());
			System.out.println("Order Entered: "+player.getOrder());
		}

	private Scanner scanner;//used to take player input
	private String key;//key e.g. B1
	private String path;//Horizontal  or Vertical
	private String order;//tile picker 
	
	/**
	 * Constructs and initialize key, path and order to null and a Scanner to get user inputs.
	 * @param no parameter
	 * @return no return 
	 */
	Player() {
		scanner= new Scanner(System.in);
		key=null;
		path=null;
		order=null;
	}
	/**
	 * Sets user key 
	 * @param no parameter
	 * @return no return 
	 */
	public void setKey () {
		System.out.println("Enter game bord key:");
		key = scanner.nextLine();
	}
	/**
	 * Gets user key 
	 * @param no parameter
	 * @return String 
	 */
	public String getKey() {
		return key;
	}
	/**
	 * Sets user path e.g. [Horizontal] or [Vertical]
	 * @param no parameter
	 * @return String 
	 */
	public void setPath() {
		do {
			System.out.println("Enter word path [Horizontal] or [Vertical]: ");
			path = scanner.nextLine();
		}while(!checkPathValidity(path));
	}
	/**
	 *This method checks path validity - [Horizontal] or [Vertical]
	 * @param path this is user input which is passed as parameter
	 * @return boolean returns true or false depending user input
	 */
	private boolean checkPathValidity(String path) {
		if(path.equalsIgnoreCase("Horizontal") || path.equalsIgnoreCase("Vertical"))
			return  true;
		System.out.println("Path can only be either [Horizontal] or [Vertical]: ");
		return false;
	}

	/**
	 * Gets user path
	 * @param no parameter
	 * @return String 
	 */
	public String getPath() {
		return path;
	}
	/**
	 * Sets user number order e.g. [1-5]
	 * @param no parameter
	 * @return String 
	 */
	public void setOrder() {
		do {
			System.out.println("Enter word order[1-5]: ");
			order = scanner.nextLine();		
		}while(!checkOrderValidity(order));
		 
	}	
	/**
	 * Gets user number order e.g. [1-5]
	 * @param no parameter
	 * @return String 
	 */
	public String getOrder() {
		return order;
	}
	/**
	 *Checks order validity. it must be [1-5]
	 * @param order This is user input which is passed as parameter
	 * @return boolean returns true or false depending user input
	 */
	private boolean checkOrderValidity(String order) {
		int binaryChecker[] = new int [5];
		int counter=0;
		boolean check=false;
		if(order.length()<1 || order.length()>5) {
			System.out.println("Order entered not valid. Length must be integer between 1-5.");
		}else {
			for(int c=0; c<=order.length()-1; c++) {
				if(Character.getNumericValue(order.charAt(c))>=1 && Character.getNumericValue(order.charAt(c))<=5) {
					binaryChecker[c]=1;
				}
				else {
					binaryChecker[c]=0;
					System.out.println("Position: " + (c+1) + " "+"invalid number");
				}
					
			}
			for(int i=0; i<binaryChecker.length-1;i++) {
				if(binaryChecker[i]==1) {
					counter++;
				}	
			}
			if(counter==binaryChecker.length-1) 
				check=true;
	}
		return check;
	}	
	
			
}
