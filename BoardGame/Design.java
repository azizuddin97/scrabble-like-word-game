import java.util.LinkedHashMap;
import java.util.Map;
/**
 * Class that allows to design the game board 
 * This includes creating a data structure using O(n) time complexity 
 * and fill with default values. and it also layouts and prints the game board
 * @author Aziz Uddin
 * @version 1.0 20/11/19
 */

public class Design {
	private Map<String, Character>  map;
	private int columnCounter=1;
	private int row=1;
	private boolean rowCounter=true;
	private boolean firstRow=true;
	private StringBuffer s;
	
	/**
	 * Constructs and initialize a LinkedHashMap named map 
	 * @param no parameter
	 * @return no return 
	 */
	Design(){
		map = new LinkedHashMap<String, Character>();//LinkedHashMap as data structure for GameBoard
		s=new StringBuffer("");
	}
	/**
	 * This method is used to refill the board with default values e.g. "-"
	 * and it also creates keys e.g. A1, B1,C1 etc. for each box of the game board 
	 * @param no parameters 
	 * @return no return 
	 */
	public void layout() {
		int row=1;
		for(int i =0; i<=100; i++) {
			char column='A';
			while(column<='J') {
				String key=Character.toString(column);
				key=key+String. valueOf(row);
				char a;
				if (key.equals("E2") || key.equals("F2") || 
						key.equals("D4") || key.equals("G4") || 
						key.equals("D7") || key.equals("G7") || 
						key.equals("E9") || key.equals("F9") ) 
					a='+';
				else 
					a='-';
				map.put(key, a);
				column++;
			}
			if(row==10)
				row=1;
			else
				row++;
		}
	}
	
	/**
	 * This method extracts the data from LinkedHashMap
	 * @param no parameters 
	 * @return no return 
	 */
	public String printHashMap() {
		s.setLength(0);//set StringBuffer length to 0
		s.append("\n");
		for(String key: map.keySet()) {
			Character character= map.get(key);
			gameBoard(key, character);
		}
		 columnCounter=1;
		row=1;
		rowCounter=true;
		firstRow=true;
		s.append("\n");
		s.append("\n");
		return s.toString();
	}
	
	/**
	 * This method prints LinkedHashMap contents using given key and element. 
	 * In addition it layouts the game board by creating letters for columns and numbers for rows
	 * @param key this is the first parameter which is the key of the LinkedHashMap
	 * @param character this is the second parameter which is the element of the LinkedHashMap
	 * @return no return 
	 */
	private void gameBoard(String key, Character character) {
		if(firstRow) {
			char letter='A';
			s.append("  ");
			while (letter<='J') {
				s.append(" "+letter);
				letter++;
			}
			firstRow=false;
			s.append("\n");
		}
		if(columnCounter<=10) {
			if(rowCounter) {
				s.append(row+"  ");
				rowCounter=false;
				row++;
			}
			s.append(character+" ");
			columnCounter++;
		}
		else {
			s.append("\n");
			if(row!=10)
				s.append(row+"  ");
			else
				s.append(row+" ");
			s.append(character+" ");
			columnCounter=2;
			row++;
		}
	}
	
	/**
	 * This method sets LinkedHashMap contents 
	 * @param key this is the first parameter which is the key of the LinkedHashMap
	 * @param character this is the second parameter which is the element of the LinkedHashMap
	 * @return no return 
	 */
	public Character setHashMap(String key, Character value) {
		return  map.put(key, value);
	}
	/**
	 * This method get LinkedHashMap content using given key.
	 * @param key This is the key for finding the required element within the LinkedHashMap
	 * @return Character it returns an element of LinkedHashMap
	 */
	public Character  getHashMap(String key) {
		if( map.get(key) != null)
			return map.get(key);
		 return  null;
	}
}