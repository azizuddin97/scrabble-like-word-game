import java.util.Random;

import java.util.Scanner;


import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
/**
 * Class that allows to control the game board 
 * @author Aziz Uddin & Taz Uddin (Codings)
 * @author Harvinder Sungooo (UML)
 * @version 1.0 20/11/19
 */

public class ControllerOne implements Controller {
	public static void main(String args[]) {
		//		ControllerOne controller = new ControllerOne()	;
		//		controller.test();
	}

	private char[] alphabet;//declare an array of chart 
	private char[]  temporaryTrack;

	private Random random;//declare random
	private Design game;//declare design 
	private boolean firstPlay=true;	//checks whether is first play or not
	private ArrayList<String> storeKeys;//store adjacent cells of the entered cell
	/**
	 * Constructs and initialize an array of char Characters and  a random variable
	 * @param no parameter
	 * @return no return 
	 */
	ControllerOne(){
		alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		random = new Random();//initialize Random
		game = new Design();//initialize Random
		game.layout();//refill board with default values
		temporaryTrack = new char[]{'-', '-', '-', '-', '-'};
		storeKeys = new ArrayList<String>(); //storKeys of letters adjacent cells
	}
	/**
	 * The following method gives random tiles 
	 * @param No parameters
	 * @return String of 5 random values
	 */
	@Override
	public String refillRack() 
	{

		for(int i = 0; i < temporaryTrack.length; i++)
		{
			if(temporaryTrack[i] == '-')
			{
				temporaryTrack[i] = alphabet[random.nextInt(26)];
			}

		}

		return String.valueOf(temporaryTrack) ;
	}

	/**
	 * the following method calls a method of Design class which prints the Game Board
	 * @param No parameters
	 * @return String of Board Game
	 */
	@Override
	public String gameState() {
		return game.printHashMap();
	}

	/**
	 * the following method calls a method of uses PLAY and DESIGN Classes to allocate letters into the Game Board
	 * @param play user inputs e.g cell, direction, and letter position
	 * @param tiles Tile Rack
	 * @return String of Board Game
	 */
	@Override

	public String play(Play play, String tiles) 
	{
		int c=0;//positions counter
		String key=play.cell();//extract starting cell
		int cell=0;//row counter

		if	(key.length()>2) 
		{	
			String temp=  Character.toString(key.charAt(1))+ Character.toString(key.charAt(2));//if keys length is higher than two it gets the second and third characters 
			cell =Integer.parseInt(temp);//and converts them into integers
		}																																														
		else	
		{
			cell= Character.getNumericValue(key.charAt(1));//gets the first position of the key 
		}																																												

		if(firstPlay==true) //first play
		{
			
			if(play.dir().equals(play.dir().DOWN)) //It inserts letters for DOWN letter by doing all the necessary checks 
			{
				if(cell==10 || cell+play.letterPositionsInRack().length()-1>10) //Checks whether the DOWN option is acceptable or not
				{
					System.err.println("Cannot go  DOWN");

				}
				else
				{
					while (c<play.letterPositionsInRack().length()) 
					{
						if ((!game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('-'))&&
								(!game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('+')))
						{
							cell++;
						}
						else
						{ 
							game.setHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell), 
									tiles.charAt(Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1));//add letters into GAME BOARD -->DOWN
							
							
							if(c==play.letterPositionsInRack().length()-1) {
								checkAdjacentTiles(key.charAt(0), cell,true,1) ;//method that checks and save if empty the adjacent cells
							}
							else {
								checkAdjacentTiles(key.charAt(0), cell,false,1) ;//method that checks and save if empty the adjacent cells
							}
							temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1] = '-';
							c++;
							cell++;
						}
					}
					
				}
			}
			else //It inserts letters for ACROSS letters by doing all the necessary checks 
			{
				if(String.valueOf(key.charAt(0)).equals("J") || (key.charAt(0))+play.letterPositionsInRack().length()-1>'J'  )  //Checks whether the ACROSS option is acceptable or not
				{
					System.err.println("Cannot go  ACROSS");
				}
				else
				{
					char letterCounter=key.charAt(0);
					while (c<play.letterPositionsInRack().length()) 
					{

						if ((!game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('-'))&&
							(!game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('+'))) {
							letterCounter=+1;
						}
						else 
						{
							game.setHashMap(String.valueOf(letterCounter)+String.valueOf(cell), 
									tiles.charAt(Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1));//add letters into GAME BOARD -->ACROSS
							

							if(c==play.letterPositionsInRack().length()-1) {
								checkAdjacentTiles(letterCounter, cell,true,2) ;//method that checks and save if empty the adjacent cells

							}
							else {
								checkAdjacentTiles(letterCounter, cell,false,2) ;//method that checks and save if empty the adjacent cells
							}
								
							
							temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1] = '-';
							c++;
							letterCounter+=1;
						}
					}
				}
			}
			firstPlay=false;
			} 
		
		else {
			
		
			
				
				if(storeKeys.contains(Character.toString(key.charAt(0))+String.valueOf(cell))==true && //Checks that entered key is available on ARRALIST
						(game.getHashMap(Character.toString(key.charAt(0))+String.valueOf(cell)).equals('-') || //and that cells is empty
								game.getHashMap(Character.toString(key.charAt(0))+String.valueOf(cell)).equals('+') 
								)) {
					if(play.dir().equals(play.dir().DOWN)) //It inserts letters for DOWN letter by doing all the necessary checks 
					{
						if(cell==10 || cell+play.letterPositionsInRack().length()-1>10) //Checks whether the DOWN option is acceptable or not
						{
						System.err.println("Cannot go  DOWN");

						}
						else
						{
							while (c<play.letterPositionsInRack().length()) 
							{
								if ((!game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('-'))&&
									(!game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('+')))
								{
									cell++;
								}
								else
								{ 
									game.setHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell), 
											tiles.charAt(Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1));//add letters into GAME BOARD -->DOWN
									
									
									if(c==play.letterPositionsInRack().length()-1) {
										checkAdjacentTiles(key.charAt(0), cell,true,1) ;//method that checks and save if empty the adjacent cells
									}
								else {
									checkAdjacentTiles(key.charAt(0), cell,false,1) ;//method that checks and save if empty the adjacent cells
								}
								temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1] = '-';
								c++;
								cell++;
							}
						}
						
					}
				}
				else//It inserts letters for ACROSS letters by doing all the necessary checks 
				{
					if(String.valueOf(key.charAt(0)).equals("J") || (key.charAt(0))+play.letterPositionsInRack().length()-1>'J'  )  //Checks whether the ACROSS option is acceptable or not
					{
						System.err.println("Cannot go  ACROSS");
					}
					else
					{
						char letterCounter=key.charAt(0);
						while (c<play.letterPositionsInRack().length()) 
						{

							if ((!game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('-'))&&
									(!game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('+')))
							{
								letterCounter+=1;
							}
							else 
							{
								game.setHashMap(String.valueOf(letterCounter)+String.valueOf(cell), 
										tiles.charAt(Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1));//add letters into GAME BOARD -->ACROSS
								
								
								if(c==play.letterPositionsInRack().length()-1) {
									checkAdjacentTiles(letterCounter, cell,true,2) ;//method that checks and save if empty the adjacent cells

								}
								else {
									checkAdjacentTiles(letterCounter, cell,false,2) ;//method that checks and save if empty the adjacent cells
								}
									
								
								temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1] = '-';
								c++;
								letterCounter+=1;
							}
						}
					}
				}
				}else {
					System.err.println("Adjacent key entered not valid");
				}
					
		}
			
//		System.out.println("\n");
//		System.out.println("Next adjacent cell : " + storeKeys);

		return game.printHashMap();


	}
	/**
	 * The following method checks if the adjacent methods are empty. If yes saves into an ArrayList to use them into subsequent PLAY.
	 * @param key LinkedHashMap key's first character e.g. A
	 * @param number LinkedHashMap key's second and third character e.g. 1,2, 10 etc.
	 * @param lastLetter A boolean that checks that checks if the sequence of letter has reached its final cell location
	 * @param option It allows to direct the operation e.g. [1 for DOWN adjacent cell checks] [2 for ACROSS adjacent cell checks]
	 */
	private void checkAdjacentTiles(char key, int number, boolean lastLetter,int option) {
		//System.out.println(game.getHashMap(String.valueOf(key)+String.valueOf(number-1)));
		
		switch(option) {
		case 1://checks adjacent cells for DOWN options
			char k=key;
			//(String.valueOf(letterCounter)+String.valueOf(cell)
			if((game.getHashMap(String.valueOf(--k)+String.valueOf(number))!=null) &&
					(game.getHashMap(String.valueOf(k)+String.valueOf(number)).equals('-') || 
					game.getHashMap(String.valueOf(k)+String.valueOf(number)).equals('+') )) {
				//System.out.println(String.valueOf(k)+String.valueOf(number));
						storeKeys.add(String.valueOf(k)+String.valueOf(number));
				}
			k=key;

			if((lastLetter && game.getHashMap(String.valueOf(k)+String.valueOf(number+1))!=null) &&		
					(game.getHashMap(String.valueOf(k)+String.valueOf(number+1)).equals('+') || 
					game.getHashMap(String.valueOf(k)+String.valueOf(number+1)).equals('-'))){
				//System.out.println(String.valueOf(k)+String.valueOf(number+1));
				storeKeys.add(String.valueOf(k)+String.valueOf(number+1));
				} 
			k=key;
			if((game.getHashMap(String.valueOf(k+=1)+String.valueOf(number))!=null) &&
					(game.getHashMap(String.valueOf(k)+String.valueOf(number)).equals('-') ||
					game.getHashMap(String.valueOf(k)+String.valueOf(number)).equals('+'))) {
					//System.out.println(String.valueOf(k)+String.valueOf(number));
					storeKeys.add(String.valueOf(k)+String.valueOf(number));
				}
			k=key;
			if((game.getHashMap(String.valueOf(k)+String.valueOf(number-1))!=null) &&
					(game.getHashMap(String.valueOf(k)+String.valueOf(number-1)).equals('-') || 
					game.getHashMap(String.valueOf(k)+String.valueOf(number-1)).equals('+'))){
					//System.out.println(String.valueOf(k)+String.valueOf(number-1));
					storeKeys.add(String.valueOf(k)+String.valueOf(number-1));
				} 
			k=key;
			break;
		case 2: //Checks adjacent cells for ACROSS option
			char k1=key;

			if((game.getHashMap(String.valueOf(--k1)+String.valueOf(number))!=null) &&
					(game.getHashMap(String.valueOf(k1)+String.valueOf(number)).equals('-') || 
					game.getHashMap(String.valueOf(k1)+String.valueOf(number)).equals('+'))){
					//System.out.println(String.valueOf(k1)+String.valueOf(number));
					storeKeys.add(String.valueOf(k1)+String.valueOf(number));
					
				} 
			k1=key;
			if
			((game.getHashMap(String.valueOf(k1)+String.valueOf(number-1))!=null) &&
					(game.getHashMap(String.valueOf(k1)+String.valueOf(number-1)).equals('-') || 
					game.getHashMap(String.valueOf(k1)+String.valueOf(number-1)).equals('+'))) {
				//System.out.println(String.valueOf(k1)+String.valueOf(number-1));
						storeKeys.add(String.valueOf(k1)+String.valueOf(number-1));
						
				}
			k1=key;
			if((game.getHashMap(String.valueOf(k1)+String.valueOf(number+1))!=null) &&
					(game.getHashMap(String.valueOf(k1)+String.valueOf(number+1)).equals('-') ||
					game.getHashMap(String.valueOf(k1)+String.valueOf(number+1)).equals('+'))) {
					//System.out.println(String.valueOf(k1)+String.valueOf(number+1));
					storeKeys.add(String.valueOf(k1)+String.valueOf(number+1));
					
				}
			k1=key;
			if((lastLetter && game.getHashMap(String.valueOf(k1+=1)+String.valueOf(number))!=null) &&
					(game.getHashMap(String.valueOf(k1)+String.valueOf(number)).equals('-') || 
					game.getHashMap(String.valueOf(k1)+String.valueOf(number)).equals('+'))){
				//System.out.println(String.valueOf(k1)+String.valueOf(number));
				storeKeys.add(String.valueOf(k1)+String.valueOf(number));
				k1=key;
				} 
			break;
			
			default:
				storeKeys=null;
			}
		}

		

	@Override
	public String calculateScore(Play play) 
	{
		int c=0;//positions counter
		String key=play.cell();//extract starting cell
		int cell=0;
		int value = 0;
		
		if	(key.length()>2) 
		{	
			String temp=  Character.toString(key.charAt(1))+ Character.toString(key.charAt(2));//if keys length is higher than two it gets the second and third characters 
			cell =Integer.parseInt(temp);//and converts them into integers
		}																																														
		else	
		{
			cell= Character.getNumericValue(key.charAt(1));//gets the first position of the key 
		}
		
		boolean anyDashes = false;
		for(int i = 0; i < temporaryTrack.length; i++)
		{
			if(temporaryTrack[i] == '-')
			{
				anyDashes = true;
				System.err.println("Refil Tiles First");
				break;
			}
		}
		
		if(play.dir().equals(play.dir().DOWN) && anyDashes == false) 
		{
			
			if(cell==10) 
			{
				System.err.println("Cannot go DOWN");
				
			}
			else
			{
				while (c<play.letterPositionsInRack().length()) 
				{
					if (!game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('-') && !game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('+'))
					{
						cell++;
					}
					else
					{ 
						if(game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('-'))
						{
							
							char temp = temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1];
				
							if(Character.toString(temp).matches("[QXYZ]"))
							{
								value = value + 3;
							}
							else if(Character.toString(temp).matches("[BGJKMN]"))
							{
								value = value + 2;
							}
							else
							{
								value = value + 1;
							}
							
						}
						else if(game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('+'))
						{
							
							char temp = temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1];
							
							if(Character.toString(temp).matches("[QXYZ]"))
							{		
								value = value + (3*2);
							}
							else if(Character.toString(temp).matches("[BGJKMN]"))
							{									
								value = value + (2*2);
							}
							else
							{
								value = value + (1*2);
							}
						}
					
					}
					c++;
					cell++;
				}
			}
		}
		
		if(play.dir().equals(play.dir().ACROSS) && anyDashes == false)
		{
			
			if(String.valueOf(key.charAt(0)).equals("J")) 
			{
				System.err.println("Cannot go ACROSS");
			}
			else
			{
				char letterCounter=key.charAt(0);
				while (c<play.letterPositionsInRack().length()) 
				{
					
					if (!game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('-') && !game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('+'))
					{
						letterCounter++;
					}
					else
					{ 
						if(game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('-'))
						{
							
							char temp = temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1];
							
							if(Character.toString(temp).matches("[QXYZ]"))
							{
								value = value + 3;
							}
							else if(Character.toString(temp).matches("[BGJKMN]"))
							{
								
								value = value + 2;
							}
							else
							{
								value = value + 1;
							}
							
						}
						else if(game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('+'))
						{
							
							char temp = temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1];
							
							if(Character.toString(temp).matches("[QXYZ]"))
							{		
								value = value + (3*2);
							}
							else if(Character.toString(temp).matches("[BGJKMN]"))
							{
								value = value + (2*2);
							}
							else
							{
								value = value + (1*2);
							}
						}
					
					}
					c++;
					letterCounter++;
					
				}
			}
		}
		return"\n"+ "Total Points: " + value;

	}
	/**
	 * The following methods extract ythe word from the GameBoard to validate it
	 * @param play containg the user inputs
	 * @param tiles used to input words
	 * @return String BoardGame
	 */
	private vo
	@Override
	public String checkValidity(Play play, String tiles) {
		int c=0;//positions counter
		String key=play.cell();//extract starting cell
		int cell=0;//row counter
		
		if	(key.length()>2) 
		{	
			String temp=  Character.toString(key.charAt(1))+ Character.toString(key.charAt(2));//if keys length is higher than two it gets the second and third characters 
			cell =Integer.parseInt(temp);//and converts them into integers
		}																																														
		else	
		{
			cell= Character.getNumericValue(key.charAt(1));//gets the first position of the key 
		}																																												
		
			if(play.dir().equals(play.dir().DOWN)) //It inserts letters for DOWN letter by doing all the necessary checks 
			{
				if(cell==10 || cell+play.letterPositionsInRack().length()-1>10) //Checks whether the DOWN option is acceptable or not
				{
					System.err.println("Cannot go  DOWN");

				}
				else
				{
					while (c<play.letterPositionsInRack().length()) 
					{
						if ((!game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('-'))&&
								(!game.getHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell)).equals('+')))
						{
							cell++;
						}
						else
						{ 
						game.setHashMap(String.valueOf(key.charAt(0))+String.valueOf(cell), 
							tiles.charAt(Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1));//add letters into GAME BOARD -->DOWN

							c++;
							cell++;
						}
					}
					wordValidity(String.valueOf(key.charAt(0)), cell, 1);
					
				}
			}
			else //It inserts letters for ACROSS letters by doing all the necessary checks 
			{
				if(String.valueOf(key.charAt(0)).equals("J") || (key.charAt(0))+play.letterPositionsInRack().length()-1>'J'  )  //Checks whether the ACROSS option is acceptable or not
				{
					System.err.println("Cannot go  ACROSS");
				}
				else
				{
					char letterCounter=key.charAt(0);
					while (c<play.letterPositionsInRack().length()) 
					{

						if ((!game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('-'))&&
								(!game.getHashMap(String.valueOf(letterCounter)+String.valueOf(cell)).equals('+')))
						{
							letterCounter+=1;
						}
						else 
						{
							game.setHashMap(String.valueOf(letterCounter)+String.valueOf(cell), 
									tiles.charAt(Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1));//add letters into GAME BOARD -->ACROSS
		
							temporaryTrack[Character.getNumericValue(play.letterPositionsInRack().charAt(c))-1] = '-';
							c++;
							letterCounter+=1;
						}
					}
					wordValidity(String.valueOf(key.charAt(0)),cell, 2);
				}
			}
		
		return game.printHashMap();

	}
	/**
	 * The following methods checks wheter a the entered word is a Valid or Invalid word
	 * @param Column letter of column
	 * @param row number of row
	 * @param option e.g ACROSS OR DOWN
	 * @return no return
	 */
	private void wordValidity(String column, int row, int option) {
		
		try {
			String temporary="";
			switch(option) {
			case 1:
				for (int i=1;i<=10;i++) {
					if ((!game.getHashMap(column+String.valueOf(i)).equals('-'))&&
							(!game.getHashMap(column+String.valueOf(i)).equals('+')))
						temporary=temporary+game.getHashMap(column+Integer.toString(i));
				}

			case 2:
				for (char i='A';i<='J';i++) {
					if ((!game.getHashMap(Character.toString(i)+String.valueOf(row)).equals('-'))&&
							(!game.getHashMap(Character.toString(i)+String.valueOf(row)).equals('+')))
						temporary=temporary+game.getHashMap(Character.toString(i)+Integer.toString(row));
				}

				
			}
			
			File file = new File("C:\\Users\\User\\eclipse-workspace\\Game\\src\\Dictionary.txt");
			Scanner scan = new Scanner(file);
			boolean wordValidity=false;
			while(scan.hasNext()) {
				String line=scan.nextLine();
				if(!line.equals(temporary)) {
					wordValidity=false;
				}else {
					wordValidity=true;
				}
			}
			if(!wordValidity) {
				System.out.println("\n");
				System.out.println("Word: "+temporary + "  is an Invalid English word");
				System.out.println("\n");
			}else {
				System.out.println("\n");
				System.out.println("Word: "+temporary+ " is a Valid English word");
				System.out.println("\n");
			}
			}
			catch(FileNotFoundException e) {
			   // there was some connection problem, or the file did not exist on the server,
			   // or  URL was not in the right format.
			   e.printStackTrace(); // for now, simply output it.
			}
		
	
		
	}
}
