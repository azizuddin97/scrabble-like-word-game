import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class ControllerOneTest {

	ControllerOne controller = new ControllerOne();
	
	@Test
	void testRefillRack_ContainsOnlyCharacters() 
	{
		
		String actual = controller.refillRack();
		boolean temp = false;
		
		for(int i = 0; i < actual.length(); i++)
		{
			if(!Character.isLetter(actual.charAt(i)))
			{
				temp = true;
			}
		}
		
		assertFalse(temp);
	}
	
	@Test
	void testRefillRack_RefilledFirstThreeCharacters()
	{
		
		String oldRack = controller.refillRack();
		Play play = new Play("D3", "DOWN", "123");
		boolean temp = false;

		controller.play(play, oldRack);
		
		String newRack = controller.refillRack();
		
		for(int i = 0; i<3; i++)
		{
			if(oldRack.charAt(i) != newRack.charAt(i))
			{
				temp = true;
			}
		}
		
		assertTrue(temp);
	}
	
	@Test
	void testGameState_ContainOnlyPlusInSpecificedCell()
	{
		Design newboard = new Design();
		newboard.layout();
		
		String[] key = {"E2", "F2", "D4", "G4", "D7", "G7", "E9", "F9"};
		boolean temp = false;
		
		
		for(int i = 0; i<key.length; i++)
		{
			if(newboard.getHashMap(key[i]) == '+')
			{
				temp = true;
			}
				
		}
		
		assertTrue(temp);
	}
	
	@Test
	void testGameStage_Boundaries()
	{
		Design newboard = new Design();
		newboard.layout();
		
		boolean temp = false;
		
		if(newboard.getHashMap("A11") == null)
		{
			temp = true;
		}
		
		if(newboard.getHashMap("A0") == null)
		{
			temp = true;
		}
		
		if(newboard.getHashMap("K1") == null)
		{
			temp = true;
		}
		
		assertTrue(temp);
	}
	
	@Test
	void testCalculateScore_CorrectCalculationNoneMultipler()
	{
		String oldRack = controller.refillRack();
		Play play = new Play("D3", "ACROSS", "123");
		String actual = controller.calculateScore(play);
		
		int actualValue = Character.getNumericValue(actual.charAt(actual.length() - 1));
		int expectedValue = 0;
		
		boolean temp = false;
		
		for (int i = 0; i < 3; i++) 
		{
			if(Character.toString(oldRack.charAt(i)).matches("[QXYZ]"))
			{
				expectedValue = expectedValue + 3;
			}
			else if(Character.toString(oldRack.charAt(i)).matches("[BGJKMN]"))
			{
				
				expectedValue = expectedValue + 2;
			}
			else
			{
				expectedValue = expectedValue + 1;
			}
		}
		
		if(actualValue == expectedValue)
		{
			temp = true;
		}
		
		assertTrue(temp);
		
	}
	
	@Test
	void testCalculateScore_CorrectCalculationWithMultipler()
	{
		String oldRack = controller.refillRack();
		Play play = new Play("E2", "ACROSS", "12");
		String actual = controller.calculateScore(play);
		
		int actualValue = Character.getNumericValue(actual.charAt(actual.length() - 1));
		int expectedValue = 0;
		
		boolean temp = false;
		
		for (int i = 0; i < 2; i++) 
		{
			if(Character.toString(oldRack.charAt(i)).matches("[QXYZ]"))
			{
				expectedValue = expectedValue + (3*2);
			}
			else if(Character.toString(oldRack.charAt(i)).matches("[BGJKMN]"))
			{
				
				expectedValue = expectedValue + (2*2);
			}
			else
			{
				expectedValue = expectedValue + (1*2);
			}
		}
		
		if(actualValue == expectedValue)
		{
			temp = true;
		}
		
		assertTrue(temp);
	}
	
	
}
